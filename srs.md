# Software Requirements Specification
## For Minesweeper 

Version 0.1  
Prepared by **Abel Salas Leal** and **Jesus Perez Perez**  
**January 28 2022**

Table of contents
=================
* [Revision History](#revision-history)
* 1 [Introduction](#1-introduction)
    * 1.1 [Document Purpose](#11-document-purpose)
    * 1.2 [Product Scope](#12-product-scope)
* 2 [Requirements](#2-requirements)
    * 2.1 [Software interfaces](#21-software-interfaces)
    * 2.2 [Design and implementation](#22-design-and-implementation)
        * 2.2.1 [Installation](#221-installation)
        * 2.2.2 [Portabililidad](#222-portability)
        * 2.2.3 [Cost](#223-cost)
        * 2.2.4 [Deadline](#224-deadline)

## Revision History
| Name | Date    | Reason For Changes  | Version   |
| ---- | ------- | ------------------- | --------- |
|      |         |                     |           |
|      |         |                     |           |
|      |         |                     |           |

## 1. Introduction
> This section should provide an overview of the entire document
### 1.1 Document Purpose
Defining how the minesweeper program is going to work, including: data, libraries and frameworks that will be used in the development process. 

### 1.2 Product Scope
The final product it's the minesweeper game, It will be developed using Typescript. The game will have a score system that everyone will be able to see.

## 2. Requirements
* Minesweeper game
* The product development process consists in 2 parts frontend y backend

#### Frontend
* At the end of the game the players will be presented with the option to save their scores, they'll need to provide a username.
* The username is the only info a player must provide
* The score will be in function of the clear time and will be sectioned by game difficulty.

###### Game difficulty
* Easy: 9x9
* Normal: 16x16
* Hard: 30x16
#### Backend
* Will take care of storing the scores in the database 
* Scores will be stores in a postgres database with only 1 table.

![](./minesweeper.png)
#### 2.1 Software interfaces
**Typescript:** Programming language to build the project's logic   
**HTML y CSS:** Markup and style languages for the design of the project's UI    
**Vite:** This is a javascript bundler that will build the project   
**Postgresql:** Type of database where the information will be stored    

### 2.2 Design and Implementation

#### 2.2.1 Installation
For the project to run on your machine, it's necessary to have node and npm installed

``
npm install  
``